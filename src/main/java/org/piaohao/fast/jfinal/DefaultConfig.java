package org.piaohao.fast.jfinal;

import com.jfinal.kit.PropKit;

/**
 * 默认的配置,服务器启动的时候会读取
 *
 * @author piaohao
 */
public class DefaultConfig {

    public static Integer serverPort;
    public static String contextPath;
    public static String staticPath;
    public static String tomcatBaseDir;
    public static String configClass;
    public static String serverType;
    public static String serverinvoke;

    public static String getPropetyAsString(String key, String defaultValue) {
        String value = System.getProperty(key);
        if (value == null)
            value = PropKit.get(key, defaultValue);
        System.out.println("properties [" + key + "]" + " = " + value);
        return value;
    }

    public static Integer getPropetyAsInteger(String key, Integer defaultValue) {
        String strValue = getPropetyAsString(key, String.valueOf(defaultValue));
        return Integer.valueOf(strValue);
    }

    public static String getPropetyAsString(String key) {
        return getPropetyAsString(key, null);
    }

    public static void init() {
        PropKit.use(Util.DEFAULT_PROPERTIES);
        serverPort = getPropetyAsInteger(Util.SERVER_PORT, 8080);
        contextPath = getPropetyAsString(Util.CONTEXT_PATH, "");
        staticPath = getPropetyAsString(Util.STATIC_PATH, "static");
        tomcatBaseDir = getPropetyAsString(Util.TOMCAT_BASE_DIR, "/tmp/tomcat");
        configClass = getPropetyAsString(Util.CONFIG_CLASS);
        serverType = getPropetyAsString(Util.SERVER_TYPE);
        serverinvoke = getPropetyAsString(Util.SERVER_INVOKE, null);
        PropKit.useless(Util.DEFAULT_PROPERTIES);
    }

}

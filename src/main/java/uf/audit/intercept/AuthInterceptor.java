package uf.audit.intercept;

import java.util.ArrayList;
import java.util.List;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;

import uf.audit.controller.UFBaseController;
import uf.audit.util.Consts;
import uf.audit.util.Utils;

/**
 * 权限拦截器
 * 
 * @author sunny
 *
 */
public class AuthInterceptor implements Interceptor {

	private static List<String> excludeStrings;

	/*
	 * 例外url配置，这些url将会被权限拦截器忽略
	 */
	static {
		excludeStrings = new ArrayList<String>();
		excludeStrings.add("/login");
		excludeStrings.add("/logout");
		excludeStrings.add("/index");
		excludeStrings.add("/js4pz");
		excludeStrings.add("/downloaddoc");
		excludeStrings.add("/getdownloaddocs");
		excludeStrings.add("/upload");
	}

	public void intercept(Invocation invocation) {
		String urlString = invocation.getControllerKey();
		String action = invocation.getActionKey();
		if (urlString.equals("/") && excludeStrings.contains(action)) {
			invocation.invoke();
			return;
		}
		UFBaseController ufbc = (UFBaseController) invocation.getController();
		if (Utils.isLogin(ufbc)) {
			boolean passAuthCheck = true;
			if (urlString.indexOf("/mgr") >= 0) {
				Boolean isadmin = (Boolean) ufbc.getSession().getAttribute(Consts.AUTH_ADMIN_STR);
				if (!isadmin) {
					passAuthCheck = false;
				}
			} else if (urlString.indexOf("/yj") >= 0) {
				Boolean ismarking = (Boolean) ufbc.getSession().getAttribute(Consts.AUTH_MARKING_STR);
				if (!ismarking) {
					passAuthCheck = false;
				}
			}
			if (passAuthCheck)
				invocation.invoke();
			else
				ufbc.renderError(401);
		} else {
			ufbc.redirect("/login");
		}
	}
}

package uf.audit.util.poi;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;

public class WordUtil {
	public static XWPFDocument generateWord(Map<String, Object> param,
			InputStream input) {
		XWPFDocument doc = null;
		try {
			doc = new XWPFDocument(input);
			if (param != null && param.size() > 0) {
				// 处理段落
				List<XWPFParagraph> paragraphList = doc.getParagraphs();
				processParagraphs(paragraphList, param, doc);
				// 处理表格，就两个表格，这里处理第一个，第二个单独处理（涉及指标增加）
				XWPFTable table = doc.getTables().get(0);
				List<XWPFTableRow> rows = table.getRows();
				for (XWPFTableRow row : rows) {
					List<XWPFTableCell> cells = row.getTableCells();
					for (XWPFTableCell cell : cells) {
						List<XWPFParagraph> paragraphListTable = cell
								.getParagraphs();
						processParagraphs(paragraphListTable, param, doc);
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return doc;
	}

	public static XWPFDocument processTable(XWPFDocument doc,
			List<String> col1s, List<String> col2s, List<String> col3s,
			List<String> col4s) {
		// 处理第二个表格
		XWPFTable table = doc.getTables().get(1);
		List<XWPFTableRow> rows = table.getRows();
		XWPFTableRow row = rows.get(1);
		for (int i = 0, j = col1s.size(); i < j; i++) {
			if (i == 0) {
				row.getCell(0).setText(col1s.get(0));
				row.getCell(1).setText(col2s.get(0));
				row.getCell(2).setText(col3s.get(0));
				row.getCell(3).setText(col4s.get(0));
			} else {
				XWPFTableRow newRow = table.createRow();
				newRow.setHeight(row.getHeight());
				CTTcPr cellPr = newRow.getCell(0).getCTTc().addNewTcPr();
				cellPr.addNewTcW().setW(
						row.getCell(0).getCTTc().getTcPr().getTcW().getW());

				cellPr = newRow.getCell(1).getCTTc().addNewTcPr();
				cellPr.addNewTcW().setW(
						row.getCell(1).getCTTc().getTcPr().getTcW().getW());

				cellPr = newRow.getCell(2).getCTTc().addNewTcPr();
				cellPr.addNewTcW().setW(
						row.getCell(2).getCTTc().getTcPr().getTcW().getW());

				cellPr = newRow.getCell(3).getCTTc().addNewTcPr();
				cellPr.addNewTcW().setW(
						row.getCell(3).getCTTc().getTcPr().getTcW().getW());

				newRow.getCell(0).setText(col1s.get(i));
				newRow.getCell(1).setText(col2s.get(i));
				newRow.getCell(2).setText(col3s.get(i));
				newRow.getCell(3).setText(col4s.get(i));
			}
		}
		return doc;
	}

	/**
	 * 处理段落
	 * 
	 * @param paragraphList
	 */
	public static void processParagraphs(List<XWPFParagraph> paragraphList,
			Map<String, Object> param, XWPFDocument doc) {
		if (paragraphList != null && paragraphList.size() > 0) {
			for (XWPFParagraph paragraph : paragraphList) {
				List<XWPFRun> runs = paragraph.getRuns();
				for (XWPFRun run : runs) {
					String text = run.getText(0);
					if (text != null) {
						boolean isSetText = false;
						for (Entry<String, Object> entry : param.entrySet()) {
							String key = entry.getKey();
							if (text.indexOf(key) != -1) {
								isSetText = true;
								Object value = entry.getValue();
								if (value instanceof String) {// 文本替换
									text = text.replace(key, value.toString());
								}
							}
						}
						if (isSetText) {
							run.setText(text, 0);
						}
					}
				}
			}
		}
	}
}

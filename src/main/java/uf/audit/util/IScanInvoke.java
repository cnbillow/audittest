package uf.audit.util;

import javax.servlet.ServletException;

/**
 * 类扫描回调函数
 *
 * @author sunny
 *
 */
public interface IScanInvoke {

    @SuppressWarnings("rawtypes")
    void invoke(ClassSearcher scan, Class clazz) throws ServletException;
}

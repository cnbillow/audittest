package uf.audit.util;

/**
 * 全局常量定义
 * 
 * @author sunny
 *
 */
public class Consts {
	public static final String DEFAULT_ENCODING = "utf-8";
	public static final String ADMIN_FIX = "--";
	public static final String SESSION_USER_ID = "session_user_id";

	public static final int AUTH_TEST = 3; // 考试卷
	public static final int AUTH_MARKING = 2; // 阅卷人员
	public static final String AUTH_MARKING_STR = "ismarking"; // 阅卷人员Key
	public static final int AUTH_ADMIN = 1; // 管理员
	public static final String AUTH_ADMIN_STR = "isadmin"; // 管理员Key

	// Session中保存的一些变量
	public static final String SESSION_USERNAME_STR = "username";
	public static final String SESSION_USERAUTH_STR = "userauths";
	
	public static final String RESET_PASSWORD = "123456";
}

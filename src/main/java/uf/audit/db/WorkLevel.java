package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;

/**
 * 职级数据实体
 * 
 * @author sunny
 *
 */
@Table(key = "levelbh")
public class WorkLevel extends Model<WorkLevel> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static WorkLevel dao = new WorkLevel().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public WorkLevel set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取所有职级
	public List<WorkLevel> getlevels() {
		return find("select * from worklevel");
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("levelbh");
		WorkLevel item = new WorkLevel();
		item.set("levelname", obj.getString("levelname"));
		boolean result = false;
		if (bh != null) {
			item.set("levelbh", obj.getString("levelbh"));
			result = item.update();
		} else {
			item.set("levelbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}

package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;

/**
 * 分类实体
 * 
 * @author sunny
 *
 */
@Table(key = "catebh")
public class Category extends Model<Category> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static Category dao = new Category().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public Category set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取所有分类
	public List<Category> getcategories() {
		return find("select * from category");
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("catebh");
		Category item = new Category();
		item.set("catename", obj.getString("catename"));
		boolean result = false;
		if (bh != null) {
			item.set("catebh", obj.getString("catebh"));
			result = item.update();
		} else {
			item.set("catebh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}

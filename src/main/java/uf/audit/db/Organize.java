package uf.audit.db;

import java.util.List;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;

/**
 * 机构数据实体
 * 
 * @author sunny
 *
 */
@Table(key = "orgbh")
public class Organize extends Model<Organize> {
	private static final long serialVersionUID = -7644540082576539659L;
	public static Organize dao = new Organize().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public Organize set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 获取所有机构
	public List<Organize> getorganizes() {
		/**
		 * orgbh varchar(50) comment '部门编号', orgname varchar(50) comment '部门名称',
		 * parentbh varchar(50) comment '父级部门'
		 */
		return find("select * from organize");
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("orgbh");
		Organize item = new Organize();
		item.set("orgname", obj.getString("orgname"));
		if (obj.getString("parentbh") != null) {
			item.set("parentbh", obj.getString("parentbh"));
		}
		boolean result = false;
		if (bh != null) {
			item.set("orgbh", obj.getString("orgbh"));
			result = item.update();
		} else {
			item.set("orgbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}

package uf.audit.db;

import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;

import uf.audit.util.Table;
import uf.audit.util.Utils;

/**
 * 考试试卷实体
 * 
 * @author sunny
 *
 */
@Table(key = "zjrdbh")
public class Zjrd extends Model<Zjrd> {
	private static final long serialVersionUID = -7644540082576539670L;
	public static Zjrd dao = new Zjrd().use(DbKit.MAIN_CONFIG_NAME);

	// 容错处理 如果set的值为空，就不做处理
	public Zjrd set(String attr, Object value) {
		if (value != null) {
			super.set(attr, value);
		}
		return this;
	}

	// 根据编号获取某个考试试卷
	public Zjrd getzjrd(String username, String modelbh) {
		return findFirst("select * from zjrd where username=? and modelbh=?", username, modelbh);
	}
	
	public Zjrd getzjrdwithbh(String userbh, String modelbh) {
		return findFirst("select * from zjrd where userbh=? and modelbh=?", userbh, modelbh);
	}

	// 更新
	public boolean update(JSONObject obj) {
		String bh = obj.getString("zjrdbh");
		Zjrd item = new Zjrd();
		item.set("userbh", obj.getString("userbh"));
		item.set("username", obj.getString("username"));
		item.set("realname", obj.getString("realname"));
		item.set("userno", obj.getString("userno"));
		item.set("yzj", obj.getString("yzj"));
		item.set("sqzj", obj.getString("sqzj"));
		item.set("pdzj", obj.getString("pdzj"));
		item.set("pwjy", obj.getString("pwjy"));
		item.set("hrjy", obj.getString("hrjy"));
		item.set("pass", obj.getInteger("pass"));
		item.set("rq", Utils.getDateTime());
		item.set("markingbh", obj.getString("markingbh"));
		item.set("modelbh", obj.getString("modelbh"));
		boolean result = false;
		if (bh != null) {
			item.set("zjrdbh", bh);
//			result = DbPro.use().update("zjrd", "zjrdbh", item);
			result = item.update();
		} else {
			item.set("zjrdbh", UUID.randomUUID().toString());
			result = item.save();
		}
		return result;
	}

	// 删除
	public boolean delete(String bh) {
		return deleteById(bh);
	}
}
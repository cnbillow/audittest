package test.audit;

import sun.net.www.http.HttpClient;
import test.audit.HttpUtils.HttpUtils;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * test for 1000 threads
 *
 * @Author duyw
 * @Date 2018-09-19
 */
public class Threads {

    public static void main(String[] args) {
        ExecutorService es = Executors.newFixedThreadPool(500);
        long start1 = new Date().getTime();
        for (int j = 1; j <= 1000; j++) {
            System.out.println(j);
            System.out.println("\n\n\n\n");
            es.execute(new Runnable() {
                @Override
                public void run() {
                    System.out.println("**************************************");
                    System.out.println("线程名称" + Thread.currentThread().getId());
                    long id = Thread.currentThread().getId();
                    long start = new Date().getTime();
                    HttpUtils httpUtils = new HttpUtils("");
                    Random random = new Random();
                    int step = random.nextInt(19);
                    String result = httpUtils.sendGet("http://127.0.0.1:8888/getmytest?userbh=" + id + "&step=" + String.valueOf(step));
                    System.out.println(String.format("----------------------------- step is %d", step));
                    System.out.println(result);
                    long end = new Date().getTime();
                    System.out.println(end - start);
                    System.out.println("**************************************");
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
        long end1 = new Date().getTime();
        System.out.println("&*&*&*&*&*&*&*&*&*&*&*&*&*&*&*&*&*&*&**" + (end1 - start1));
    }
}
